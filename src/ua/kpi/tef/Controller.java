package ua.kpi.tef;

import java.util.Scanner;

public class Controller {
    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model  = model;
        this.view = view;
    }

    public void processUser(){
        Scanner sc = new Scanner(System.in);
        model.addWord(checkInput(sc,"Hello"));
        model.addWord(checkInput(sc,"world!"));
        view.printMessage(model.getSentence());
    }

    private String checkInput(Scanner sc, String word) {
        view.printMessage(View.INPUT_REQ);
        String result = sc.nextLine();
        while(!result.equals(word)) {
            view.printMessage(View.WRONG_INPUT);
            result = sc.nextLine();
        }
        return result;
    }
}
