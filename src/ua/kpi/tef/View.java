package ua.kpi.tef;

public class View {
    public static final String INPUT_REQ = "Input your word, please:";
    public static final String WRONG_INPUT = "Wrong input! Repeat please:";

    public void printMessage(String message){
        System.out.println(message);
    }
}
