package ua.kpi.tef;

public class Model {
    private StringBuilder sb = new StringBuilder();

    public void addWord(String sentence) {
        this.sb.append(sentence).append(" ");
    }

    public String getSentence() {
        return this.sb.toString().trim();
    }
}
